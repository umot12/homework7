package homework7;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Human john = new Man("John","Golovach",1955);
    Human karla = new Woman("Karla","Golovach",1945);
    Family familyWick = new Family(karla, john);
    Family family = new Family(new Man(),new Woman());

    Human Oly = new Woman("Oly","Golovach",1980);
    Woman Vika = new Woman("Vika","Golovach",2001);
    ArrayList<Human> children = new ArrayList<Human>(List.of(Vika,Oly));


    Human father = john;
    Human mother = karla;


    @Test
    void testToString() {
        familyWick.setChildren(children);
        String actual = familyWick.toString();
        String expected = "Family: mother: " + mother.getName() + " " + mother.getSurname() + ", " + "father: " + father.getName() + " " + father.getSurname() + ", children: " + children;
        assertEquals(expected,actual);
    }

    @Test
    void testDeleteChildCorrectName() {
        familyWick.setChildren(children);
        familyWick.deleteChild(Oly);
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void testDeleteChildInCorrectName() {
        familyWick.setChildren(children);
        Human child = new Man("Oleg","Golovach",30);
        familyWick.deleteChild(child);
        assertEquals(2,familyWick.getChildren().size());
    }


    @Test
    void testdeleteChildInCorrectIndex() {
        familyWick.setChildren(children);
        familyWick.deleteChild(3);
        assertEquals(2,familyWick.getChildren().size());
    }

    @Test
    void testdeleteChildCorrectIndex() {
        familyWick.setChildren(children);
        familyWick.deleteChild(0);
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void testdeleteChildInCorrectIndexBoolean() {
        familyWick.setChildren(children);
        assertEquals(false,familyWick.deleteChild(3));
    }

    @Test
    void testdeleteChildCorrectIndexBoolean() {
        familyWick.setChildren(children);
        assertEquals(true,familyWick.deleteChild(1));
    }

    @Test
    void testAddChild() {
        familyWick.setChildren(children);
        Human child = new Man("Oleg","Golovach",30);
        familyWick.addChild(child);
        assertEquals(3,familyWick.getChildren().size());
    }

    @Test
    void testAddChildCheckObject() {
        familyWick.setChildren(children);
        Human child = new Man("Oleg","Golovach",30);
        familyWick.addChild(child);
        assertEquals(true,child.equals(familyWick.getChildren().get(2)));
    }

    @Test
    void testCountFamily() {
        familyWick.setChildren(children);
        assertEquals(4,familyWick.getChildren().size() + 2);
    }

    @Test
    void testHashcode(){
        assertTrue(family.hashCode()!=familyWick.hashCode());
    }

    @Test
    void testEqualsFalse(){
        assertFalse(family.equals(familyWick));
    }

    @Test
    void testEqualsTrue(){
        assertTrue(familyWick.equals(familyWick));
    }

    //contract Equals
    @Test
    void testSymmetry() {
        assertEquals(family.equals(familyWick),familyWick.equals(family));
    }

    @Test
    void testTransitivity() {
        Family family2 = new Family(new Man(),new Woman());
        boolean[] test = {familyWick.equals(family),family.equals(family2),familyWick.equals(family2)};
        assertEquals(Arrays.toString(new boolean[]{false,false,false}),Arrays.toString(test));
    }

    @Test
    void testConsistency() {
        boolean firstInvoke = familyWick.equals(family);
        boolean secondInvoke = familyWick.equals(family);
        assertEquals(firstInvoke,secondInvoke);
    }

    @Test
    void testNull() {
        assertEquals(false,familyWick.equals(null));
    }




}
